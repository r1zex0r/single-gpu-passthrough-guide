# Single GPU Passthrough Guide

Ubuntu 20.04 Nvidia Single GPU Passthrough Guide with Intel CPU using GDM

## Getting started

Follow steps 1-6 https://gitlab.com/risingprismtv/single-gpu-passthrough/-/wikis/2)-Editing-GRUB
Remember to add `video=efifb:off`

You can use virtio disk

Ubuntu uses apparmour so make sure patched vbios is in the correct folder /usr/share blah blah which is in the guide above.

## Scripts

clone and follow instructions https://github.com/wabulu/Single-GPU-passthrough-amd-nvidia

Uncomment `killall gdm-x-session` in `start.sh`

## Audio

Plug ur headphones via the monitor jack and install nvidia drivers.
